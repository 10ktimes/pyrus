#!/bin/bash

if type -t python3 >/dev/null; then
    python3 -m venv env
else
    virtualenv env
fi

env/bin/pip install -r requirements.txt
