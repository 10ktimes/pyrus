#!/usr/bin/env python
import os
import sys
import shutil
import pathlib
import argparse

from slugify import slugify

from .template import Template
from .parser import Parser, parse_markdown


def die(msg, status=1):
    sys.stderr.write(msg + '\n')
    sys.exit(status)


def argparse_path_type(string):
    path = pathlib.Path(string)
    if not path.exists():
        raise argparse.ArgumentTypeError("path %r does not exist" % string)
    return path


if __name__ == '__main__':
    argp = argparse.ArgumentParser(description="markdown to static html generator")
    argp.set_defaults(template='templates/default', config='_config.md')
    argp.add_argument('path', type=argparse_path_type,
            help='path to markdown file or dir')

    args = argp.parse_args()

    parser = Parser()

    tmplpath = args.path
    if args.path.is_file():
        tmplpath = args.path.parent

    template = Template(tmplpath / 'template')

    #now lets get our entries
    entries = parser.entries(args.path)
    if not len(entries):
        die(':: No entries to process', 0)

    generated = template.render_entries(entries)

    # list pages
    if args.path.is_dir() and not (args.path / 'index.md').exists():
        generated.update(template.render_listpage(args.path, 'index', entries))

    tag_entries = parser.by_tag(entries)
    for tag, ents in tag_entries.items():
        tag_slug = slugify(tag)
        if not (args.path / f'{tag_slug}.md').exists():
            generated.update(template.render_listpage(args.path, tag_slug, ents))

    if args.path.is_dir() and not (args.path / 'tags.md').exists():
        all_tags = parser.all_tags(tag_entries)
        generated.update(template.render_listpage(args.path, 'tags', all_tags))

    if len(generated):
        print(f':: writing {len(generated)} pages to disk')
        for g in generated:
            print(f"  {g} -> {len(generated[g])} bytes")
            g.write_text(generated[g], encoding='utf-8', errors='xmlcharrefreplace')
    else:
        print(':: no pages generated. nothing to do')
