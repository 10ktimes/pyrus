import markdown
from slugify import slugify
from markdown.extensions.wikilinks import WikiLinkExtension, build_url

def parse_markdown(filepath):
    parsed = {
        'links': []
    }

    def _linktracker(label, base, end):
        label_slug = slugify(label)
        parsed['links'].append(label_slug)
        return '{}{}{}'.format(base, label_slug, end)

    md = markdown.Markdown(extensions=[
        'meta',
        'pymdownx.caret',
        'pymdownx.emoji',
        'pymdownx.extra',
        'pymdownx.magiclink',
        'pymdownx.smartsymbols',
        'pymdownx.tabbed',
        'pymdownx.tilde',
        'smarty',
        WikiLinkExtension(build_url=_linktracker, end_url='.html'),
    ])

    parsed['body'] = md.convert(filepath.read_text())

    # Copy Meta entries, and pull strings out of single item lists
    for k, v in md.Meta.items():
        if isinstance(v, list) and len(v) == 1:
            parsed[k] = v[0]

    # remove any keys with empty values
    return dict(filter(lambda item: item[1], parsed.items()))


