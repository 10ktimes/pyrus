import os
import sys

from datetime import datetime
from operator import itemgetter
from collections import defaultdict
from slugify import slugify
from dateutil import parser as DateParser

from .markdown import parse_markdown


class Parser:
    def __init__(self):
        pass

    def parse_page(self, filepath):
        """This method opens a markdown file using an email parser
        which expects key: value pairs followed by a blank line before
        the payload. Those k:v pairs are metadata for our pages..."""

        entry = parse_markdown(filepath)

        # our entry dict contains a bunch of useful stuff we might want in templates
        entry['filename'] = filepath.name
        entry['path'] = filepath.parent
        entry['selflink'] = slugify(filepath.with_suffix('').name) + '.html'
        entry['htmlpath'] = entry['path'] / entry['selflink']

        if 'title' not in entry:
            entry['title'] = entry.get('subject', entry['filename'])

        if 'tags' in entry:
            entry['tags'] = [slugify(t.strip()) for t in entry.get('tags', '').split(',')]

        if 'created' in entry:
            entry['created'] = DateParser.parse(entry['created'])
        elif 'date' in entry:
            entry['created'] = DateParser.parse(entry['date'])
        else:
            entry['created'] = datetime.fromtimestamp(filepath.stat().st_ctime)

        if 'modified' in entry:
            entry['modified'] = DateParser.parse(entry['modified'])
        elif 'lastmodified' in entry:
            entry['modified'] = DateParser.parse(entry['lastmodified'])
        else:
            entry['modified'] = datetime.fromtimestamp(filepath.stat().st_mtime)

        # output format
        entry['created'] = entry['created'].date().isoformat()
        entry['modified'] = entry['modified'].date().isoformat()

        return entry

    def _with_backlinks(self, entries):
        """This method finds all internal links in each entry and
        generates a list of backlinks to be used in templates"""

        backlinks = defaultdict(list)
        for ent in entries:
            # links come from the markdown parser as it hits [[x]] internal links
            for link in ent.get('links', []):
                backlinks[link].append(ent)

        for ent in entries:
            blinked = backlinks.get(ent['title'])
            if blinked:
                ent['backlink_entries'] = blinked

        return entries

    def entries(self, path):
        entries = []
        curfile = None

        if path.is_file():
            return [self.parse_page(path)]

        for f in path.rglob('*.md'):
            if f.name.startswith('_'): continue
            if f.is_file():
                entries.append(self.parse_page(f))

        entries = sorted(entries, key=itemgetter('created'), reverse=True)
        cnt = len(entries)
        for ent in entries:
            ent['id'] = cnt
            cnt -= 1

        return self._with_backlinks(entries)

    def by_tag(self, entries):
        """this method generates an index page for each tag"""
        tagged_entries = defaultdict(list)
        for ent in entries:
            for tag in ent.get('tags', []):
                tagged_entries[tag].append(ent)

        return { k: sorted(v, key=itemgetter('modified'), reverse=True) for k,v in tagged_entries.items() }

    def all_tags(self, tagged_entries):
        tag_entries = []
        for tag, entries in tagged_entries.items():
            tag_entries.append({
                'selflink': f"{tag}.html",
                'title': f"{tag} ({len(entries)})",
                'modified': entries[0]['modified']
            })
        return tag_entries
