import pathlib
from string import Template as StringTemplate
from .markdown import parse_markdown

class TemplateException(Exception):
    pass

class Template:
    def __init__(self, path):
        self.config = self._load_config(path.parent / '_config.md')
        self.header_tmpl = self._from_file(path / 'header.html')
        self.entry_tmpl = self._from_file(path / 'entry.html')
        self.listitem_tmpl = self._from_file(path / 'listitem.html')
        self.footer_tmpl = self._from_file(path / 'footer.html')

    def _load_config(self, cfgpath):
        config = {}
        if cfgpath.exists():
            config = parse_markdown(cfgpath)
            config = { f'config_{k}': v for k,v in config.items() }

        return config

    def _generate_taglinks(self, entry):
        if 'tags' in entry:
            entry['taglinks'] = ', '.join([
                '<a href="{tag}.html">{tag}</a>'.format(tag=tag)
                for tag in entry['tags']
            ])
        else:
            entry['taglinks'] = 'none'

    def _generate_backlinks(self, entry):
        if 'backlink_entries' in entry:
            entry['backlinks'] = ', '.join([
                '<a href="{link}">{text}</a>'.format(link=blent['selflink'], text=blent['title'])
                for blent in entry['backlink_entries']
            ])
            print(f"  {entry['title']} has backlinks: {entry['backlinks']}")
        else:
            entry['backlinks'] = 'none'

    def _from_file(self, filepath):
        if not filepath.exists():
            raise RuntimeError(f"Invalid template dir, {filepath} not found")
        return StringTemplate(filepath.read_text(encoding='utf-8'))

    def _sub(self, tmpl, pagetitle, entry=None):
        return tmpl.safe_substitute(
            { **self.config, **(entry or {}) },
            pagetitle=pagetitle
        )

    def render_listpage(self, path, title, entries):
        out = [self._sub(self.header_tmpl, title)]
        for ent in entries:
            out.append(self._sub(self.listitem_tmpl, title, ent))
        out.append(self._sub(self.footer_tmpl, title))

        return { path / f'{title}.html': ''.join(out) }

    def render_entries(self, entries):
        generated = {}
        for ent in entries:
            self._generate_taglinks(ent)
            self._generate_backlinks(ent)
            generated[ent['htmlpath']] = ''.join([
                self._sub(self.header_tmpl, ent['title']),
                self._sub(self.entry_tmpl, ent['title'], ent),
                self._sub(self.footer_tmpl, ent['title'])
            ])

        return generated
