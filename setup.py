#!/usr/bin/env python
import setuptools

reqs = [r.strip() for r in open('requirements.txt', 'r').readlines()]

setuptools.setup(
    name='pyrus',
    version='0.3.0',
    description='my personal static site builder in under 300 lines',
    author='Aaron Griffin',
    author_email='aaronmgriffin@gmail.com',
    url='https://gitlab.com/ten-thousand-times/pyrus',
    packages=setuptools.find_packages(),
    python_requires='>=3.9',
    install_requires=reqs
)
